package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.VODContent
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetPopularContentUseCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(count: Int = 30, page: Int = 0): List<VODContent> = withContext(Dispatchers.IO) {
        vodRepository.getPopularContent(count, page)
    }
}