package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.MovieDetails
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetMovieDetailsUseCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(movieId: String): MovieDetails = withContext(Dispatchers.IO) {
        vodRepository.getMovieDetails(movieId)
    }
}