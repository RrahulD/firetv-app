package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.ShowDetails
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetShowDetailsCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(showId: String): ShowDetails = withContext(Dispatchers.IO) {
        vodRepository.getShowDetails(showId)
    }
}