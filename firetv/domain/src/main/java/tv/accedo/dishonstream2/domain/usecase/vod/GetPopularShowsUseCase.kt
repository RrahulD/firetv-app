package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.Show
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetPopularShowsUseCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(count: Int, page: Int): List<Show> = withContext(Dispatchers.IO) {
        vodRepository.getPopularShows(count, page)
    }
}