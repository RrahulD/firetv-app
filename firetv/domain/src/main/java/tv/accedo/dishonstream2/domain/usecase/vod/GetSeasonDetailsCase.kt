package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.EpisodeDetail
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetSeasonDetailsCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(showId: String, seasonNumber: Int): List<EpisodeDetail> = withContext(Dispatchers.IO) {
        vodRepository.getSeasonDetails(showId, seasonNumber)
    }
}