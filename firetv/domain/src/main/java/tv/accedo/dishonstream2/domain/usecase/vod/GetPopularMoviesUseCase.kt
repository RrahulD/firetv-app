package tv.accedo.dishonstream2.domain.usecase.vod

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.vod.Movie
import tv.accedo.dishonstream2.domain.repository.VODRepository

class GetPopularMoviesUseCase(
    private val vodRepository: VODRepository
) {
    suspend operator fun invoke(count: Int, page: Int): List<Movie> = withContext(Dispatchers.IO) {
        vodRepository.getPopularMovies(count, page)
    }
}