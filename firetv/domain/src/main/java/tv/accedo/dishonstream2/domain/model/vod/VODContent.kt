package tv.accedo.dishonstream2.domain.model.vod


sealed class VODContent

data class Movie(
    val id: String,
    val title: String,
    val posterImage: String
) : VODContent()

data class Show(
    val id: String,
    val title: String,
    val posterImage: String
) : VODContent()
