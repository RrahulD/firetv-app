package tv.accedo.dishonstream2.domain.usecase.channel

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import tv.accedo.dishonstream2.domain.model.supair.Channel
import tv.accedo.dishonstream2.domain.repository.DishRepository

class GetEPGUseCase(private val repository: DishRepository) {
    suspend operator fun invoke(): List<Channel> = withContext(Dispatchers.IO) {
        repository.getEpg()
    }
}