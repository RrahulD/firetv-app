package tv.accedo.dishonstream2.ui.main.home.component.widget.moreinfo

import android.util.TypedValue
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import com.bumptech.glide.Glide
import tv.accedo.dishonstream2.R
import tv.accedo.dishonstream2.databinding.WidgetMoreInfoLayoutBinding
import tv.accedo.dishonstream2.domain.model.home.template.widget.MoreInfoWidget
import tv.accedo.dishonstream2.extensions.show
import tv.accedo.dishonstream2.ui.main.home.component.widget.base.WidgetViewHolder
import tv.accedo.dishonstream2.ui.main.home.dialog.MoreInfoDialogFragment

class MoreInfoWidgetViewHolder(
    private val binding: WidgetMoreInfoLayoutBinding,
    private val fragmentManager: FragmentManager
) : WidgetViewHolder(binding.root) {

    override val focusView = binding.container

    fun populate(widget: MoreInfoWidget) {
        with(binding) {
            moreInfoTitle.text = widget.title
            moreInfoDescription.text = widget.subTitle
            Glide.with(root)
                .load(widget.headerImage.fileUrl)
                .into(moreInfoPoster)
            Glide.with(root)
                .load(widget.icon.fileUrl)
                .into(moreInfoLogo)

            container.setOnClickListener {
                MoreInfoDialogFragment.newInstance(widget).show(fragmentManager)
            }
            content.show()
        }
    }

    fun setZoom() {
        binding.moreInfoTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        binding.moreInfoDescription.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18f)

        binding.moreInfoLogo.layoutParams.width = binding.root.resources.getDimension(R.dimen._50dp).toInt()
        binding.moreInfoLogo.layoutParams.height = binding.root.resources.getDimension(R.dimen._50dp).toInt()

        binding.errorTitle.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
        binding.moreInfoDescription.maxLines = 1

        (binding.root.layoutParams as ViewGroup.MarginLayoutParams).height =
            binding.root.resources.getDimension(R.dimen._500dp).toInt()
        (binding.moreInfoTextContainer.layoutParams as ViewGroup.MarginLayoutParams).height =
            binding.root.resources.getDimension(R.dimen._95dp).toInt()
    }
}