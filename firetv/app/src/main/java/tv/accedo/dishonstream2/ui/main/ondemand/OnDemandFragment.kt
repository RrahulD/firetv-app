package tv.accedo.dishonstream2.ui.main.ondemand

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import org.koin.androidx.viewmodel.ext.android.viewModel
import tv.accedo.dishonstream2.databinding.OnDemandFragmentBinding
import tv.accedo.dishonstream2.ui.base.BaseFragment

class OnDemandFragment : BaseFragment() {

    private lateinit var binding:OnDemandFragmentBinding
    private val vm: OnDemandViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = OnDemandFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
    }

    companion object {
        fun newInstance() = OnDemandFragment()
    }

}