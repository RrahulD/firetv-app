package tv.accedo.dishonstream2.ui.main.tvguide

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import tv.accedo.dishonstream2.domain.model.supair.Channel
import tv.accedo.dishonstream2.domain.model.supair.Program
import tv.accedo.dishonstream2.domain.usecase.channel.GetDRMTokenUseCase
import tv.accedo.dishonstream2.domain.usecase.channel.GetEPGUseCase
import tv.accedo.dishonstream2.domain.usecase.player.GetLicenseDetailsUseCase
import tv.accedo.dishonstream2.domain.usecase.settings.appsettings.tvguide.GetTVGuideStyleUseCase
import tv.accedo.dishonstream2.ui.base.BaseViewModel
import tv.accedo.dishonstream2.ui.main.tvguide.player.model.PlaybackInfo

class TVGuideViewModel(
    private val getLicenseDetailsUseCase: GetLicenseDetailsUseCase,
    private val getDRMTokenUseCase: GetDRMTokenUseCase,
    private val getEPGUseCase: GetEPGUseCase,
    private val getTVGuideStyleUseCaseV2: GetTVGuideStyleUseCase,
) : BaseViewModel() {

    private val tvGuideStyleLiveData: MutableLiveData<String> = MutableLiveData()
    private val useLightThemeLiveData: MutableLiveData<Boolean> = MutableLiveData(false)

    private val _epgLiveData = MutableLiveData<List<Channel>>()
    val epgLiveData: LiveData<List<Channel>> = _epgLiveData

    private val _pipPlaybackInfo = MutableLiveData<PlaybackInfo>()
    val pipPlaybackInfo: LiveData<PlaybackInfo> = _pipPlaybackInfo

    fun getEpg() {
        val exceptionHandler = CoroutineExceptionHandler { _, exception ->
            onLoading(false)
            onError(exception)
        }

        viewModelScope.launch(exceptionHandler) {
            onLoading(true)
            val tvGuideStyleDeferred = async { getTVGuideStyleUseCaseV2() }
            val epgDeferred = async { getEPGUseCase() }

            tvGuideStyleLiveData.value = tvGuideStyleDeferred.await()
            val epgData = epgDeferred.await()
            onLoading(false)
            if (epgData.isNotEmpty())
                _epgLiveData.value = epgData
            else
                onError(RuntimeException("Error while loading the TV Guide!"))
        }
    }

    fun playContentInPip(channel: Channel, program: Program) {
        viewModelScope.launch {
            onLoading(true)
            try {
                val licenseDetail = getLicenseDetailsUseCase()
                val drm = getDRMTokenUseCase(channel.contentId)
                onLoading(false)
                _pipPlaybackInfo.value = PlaybackInfo(
                    playerLicense = licenseDetail.bitmovinLicenceKey,
                    drmLicenseURL = licenseDetail.widewineLicenseUrl,
                    drmToken = drm.drmToken,
                    hlsURL = drm.serviceUrlHLS,
                    dashURL = drm.serviceUrlDASH,
                    program.startTime,
                    program.endTime
                )
            } catch (ex: Exception) {
                onLoading(false)
                onError(ex)
            }
        }
    }

    fun styleEPG(): LiveData<String> {
        return tvGuideStyleLiveData
    }

    fun useLightTheme(): LiveData<Boolean> {
        return useLightThemeLiveData
    }
}