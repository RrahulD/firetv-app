package tv.accedo.dishonstream2.ui.main.settings.parentalcontrols

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import tv.accedo.dishonstream2.R
import tv.accedo.dishonstream2.ui.base.BaseFragment

class ParentalControlsFragment : BaseFragment() {

    companion object {
        fun newInstance() = ParentalControlsFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.parental_controls_fragment, container, false)
    }
}