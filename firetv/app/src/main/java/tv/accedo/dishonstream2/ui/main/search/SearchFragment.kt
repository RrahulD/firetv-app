package tv.accedo.dishonstream2.ui.main.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatButton
import androidx.core.widget.addTextChangedListener
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import tv.accedo.dishonstream2.R
import tv.accedo.dishonstream2.databinding.LayoutButtonsBinding
import tv.accedo.dishonstream2.databinding.SearchFragmentBinding
import tv.accedo.dishonstream2.extensions.hide
import tv.accedo.dishonstream2.extensions.show
import tv.accedo.dishonstream2.ui.base.BaseFragment


class SearchFragment : BaseFragment(), View.OnClickListener {

    private lateinit var binding: SearchFragmentBinding
    private val searchViewModel: SearchViewModel by viewModel()
    private var inputConnection: InputConnection? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        binding = SearchFragmentBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.deleteRecentSearchBtn.setOnClickListener(this)
        inputConnection = binding.searchEditText.onCreateInputConnection(EditorInfo())
        binding.keyboardLayout.inputConnection = inputConnection
        binding.keyboardLayout.requestFocus()
        binding.searchEditText.addTextChangedListener {
            val searchField = it?.toString()
            if (searchField?.isNotEmpty() == true && searchField.length > 2) {
                Timber.tag(SearchViewModel.TAG).i("perform search : $searchField")
                searchViewModel.performSearch(searchField)
            }
        }
        searchViewModel.recentSearchLiveData.observe(viewLifecycleOwner) { recentSearches ->
            binding.recentSearchBtnLayout.removeAllViews()
            val lineaLayout = LinearLayout(requireContext())
            lineaLayout.layoutParams =
                LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, resources.getDimension(R.dimen._50dp).toInt())
            lineaLayout.orientation = LinearLayout.HORIZONTAL
            recentSearches.forEach {
                lineaLayout.addView(getBtnView(it))
            }
            binding.recentSearchBtnLayout.addView(lineaLayout)
            if (recentSearches.isNotEmpty()) {
                binding.recentSearchesText.show()
                binding.recentSearchParent.show()
            } else {
                binding.recentSearchesText.hide()
                binding.recentSearchParent.hide()
            }
        }
    }

    private fun getBtnView(btnText: String): AppCompatButton {
        val binding = LayoutButtonsBinding.inflate(layoutInflater)
        binding.recentSearchBtns.height = resources.getDimension(R.dimen._40dp).toInt()
        binding.recentSearchBtns.text = btnText
        binding.recentSearchBtns.setOnClickListener(this)
        return binding.root
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.recentSearchBtns -> {
                val selectedBtn: AppCompatButton = v as AppCompatButton
                binding.searchEditText.setText("")
                inputConnection?.commitText(selectedBtn.text, selectedBtn.text.length)
            }
            binding.deleteRecentSearchBtn.id -> {
                binding.searchEditText.setText("")
                searchViewModel.clearRecentSearches()
                binding.keyboardLayout.requestFocus()
            }
            else -> {
                // No action required
            }
        }
    }

    companion object {
        fun newInstance() = SearchFragment()
    }

}