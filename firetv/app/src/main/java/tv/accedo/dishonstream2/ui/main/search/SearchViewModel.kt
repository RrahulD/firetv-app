package tv.accedo.dishonstream2.ui.main.search

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import timber.log.Timber
import tv.accedo.dishonstream2.domain.usecase.search.GetRecentSearchFieldsUseCase
import tv.accedo.dishonstream2.domain.usecase.search.SetRecentSearchFieldsUseCase
import tv.accedo.dishonstream2.ui.base.BaseViewModel

class SearchViewModel(
    private val getRecentSearchFieldsUseCase: GetRecentSearchFieldsUseCase,
    private val setRecentSearchFieldsUseCase: SetRecentSearchFieldsUseCase
) : BaseViewModel() {

    private val recentSearchMutableLiveData: MutableLiveData<List<String>> = MutableLiveData()
    val recentSearchLiveData: LiveData<List<String>> = recentSearchMutableLiveData

    init {
        getRecentSearchFields()
    }

    fun performSearch(keyword: String) {
        // store the search query in keystore for recent searches
        setRecentSearchFields(keyword)
        // perform search
    }

    private fun setRecentSearchFields(value: String) {
        viewModelScope.launch {
            try {
                val recentSearches = getRecentSearchFieldsUseCase()
                val newRecentSearch = LinkedHashSet<String>()
                newRecentSearch.add(value)
                if (recentSearches.isNullOrEmpty()) {
                    setRecentSearchFieldsUseCase(newRecentSearch)
                } else {
                    newRecentSearch.addAll(recentSearches.take(MAX_RECENT_SEARCH_ITEM_INDEX))
                    setRecentSearchFieldsUseCase(newRecentSearch)
                }
                getRecentSearchFields()
            } catch (ex: Exception) {
                Timber.tag(TAG).e("Exception in setRecentSearchFields() : $ex")
            }
        }
    }

    private fun getRecentSearchFields() {
        viewModelScope.launch {
            try {
                getRecentSearchFieldsUseCase()?.let {
                    recentSearchMutableLiveData.value = it.toList()
                }

            } catch (ex: Exception) {
                Timber.tag(TAG).e("Exception in getRecentSearchFields() : $ex")
            }
        }
    }

    fun clearRecentSearches() {
        viewModelScope.launch {
            try {
                setRecentSearchFieldsUseCase(LinkedHashSet())
                getRecentSearchFields()
            } catch (ex: Exception) {
                Timber.tag(TAG).e("Exception in clearRecentSearches() : $ex")
            }
        }
    }

    companion object {
        const val TAG = "SearchFragment :: "
        private const val MAX_RECENT_SEARCH_ITEM_INDEX = 4
    }
}