package tv.accedo.dishonstream2.data.model.reelgoodvod.base


import com.google.gson.annotations.SerializedName

data class Links(
    val android: String
)